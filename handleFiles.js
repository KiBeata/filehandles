const express = require("express");
const app = express();
var multer = require('multer')
var cors = require('cors');
const sharp = require('sharp');
const hostname = '127.0.0.1';
const port = 5000;
const path = require('path');
const fs = require('fs');

var allowlist = ['http://localhost:3000/', 'http://localhost:5000/'];
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (allowlist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { origin: true }
  } else {
    corsOptions = { origin: false }
  }
  callback(null, corsOptions)
}

//app.use('*', cors(corsOptionsDelegate));

app.options('http://localhost:3000/', cors());
//allow OPTIONS on all resources
app.options('*', cors( {origin: '*'} ));

app.use(cors({ origin: '*', creadentials: false, exposedHeaders: '*' }));

//    app.use(function(req, res, next) {
//      res.setHeader('X-Frame-Options', 'ALLOWALL');
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Allow-Methods', 'POST, GET');
//     res.setHeader('Access-Control-Allow-Headers', 'Origin, Content-Length, X-Requested-With, Content-Type, Accept');
// });
  //  res.header("Access-Control-Allow-Origin", "*");
//Authorization, Cache-Control, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers
  //   res.header("Authorization, Cache-Control, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
  //   next();
  // });
  //

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, __dirname.substr(0, __dirname.length-11)+"paperwork/public/imgs/1");
    },
    filename: function (req, file, cb) {
      const basedir = __dirname.substr(0, __dirname.length-11)+"paperwork/public/imgs/1";
      var index = fs.readdirSync(basedir).length / 2;
      if(index<1) {
        index = 1;
      }
      const indexName = file.originalname.substr(file.originalname.lastIndexOf('.'));
      cb(null, 'pic-' + file.originalname );
      cb(null, 'pic-' + index + indexName );
    }

});

const fileFilter = (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

var upload = multer({ storage: storage, fileFilter: fileFilter }).single('file');



app.listen(port, () => {
  console.log(`Server app listening on port ${port}!`)
});

// app.post('/api/upload', upload.single('file'), (req, res, next) => {
//     try {
//         return res.status(201).json({
//             message: 'File uploded successfully'
//         });
//     } catch (error) {
//         console.error(error);
//     }
// });

app.post('/api/upload', function(req, res, next) {
    upload(req, res, function (err) {
           if (err instanceof multer.MulterError) {
               return res.status(500).json(err)
           } else if (err) {
               return res.status(500).json(err)
           }
      return res.status(200).send(req.file);
    })
  });

  app.delete('/api/delete/all', function(req, res, next) {
    var basedir = __dirname.substr(0, __dirname.length-11)+"paperwork/public/imgs/1";
    fs.readdir(basedir, function (err, files) {
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        }
        files.forEach(function (file) {
            console.log(file);
            fs.unlink(basedir+"/"+file, (err) => {
              console.log(err);
            });
        });
    });
  });
